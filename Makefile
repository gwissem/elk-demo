.DEFAULT_GOAL := help
.PHONY: clean deploy help

DOCKER_COMPOSE=docker-compose
TOOLS_1=$(DOCKER_COMPOSE) run --rm php_1
TOOLS_2=$(DOCKER_COMPOSE) run --rm php_2
MYSQL=$(DOCKER_COMPOSE) run --rm mysql
WAIT=$(DOCKER_COMPOSE) run --rm wait
ELK=$(DOCKER_COMPOSE) run --rm elk

# Add env variables
# $(shell cp -n .env.dist .env)
# include .env
# export $(shell sed 's/=.*//' .env)

start: docker-build docker-up init-db ## Install and start the project

stop: ## Remove docker containers
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) rm -v --force

restart: stop start ## Restart containers

docker-build:
	$(DOCKER_COMPOSE) pull --parallel --ignore-pull-failure
	$(DOCKER_COMPOSE) build

docker-up:
	$(DOCKER_COMPOSE) up -d --remove-orphans

docker-wait:
	$(WAIT) -c elk:9200
	$(WAIT) -c elk:5601
	$(WAIT) -c elk:5044 -t 60

init-db:
	$(WAIT) -c mysql:3306
	$(TOOLS_1) bin/console doctrine:database:create --if-not-exists
	$(TOOLS_1) bin/console doctrine:schema:update --force --dump-sql

vendors: ## Install vendors
	$(TOOLS_1) composer install -vvv --prefer-dist --no-interaction --optimize-autoloader
	$(TOOLS_2) composer install -vvv --prefer-dist --no-interaction --optimize-autoloader

setup-elk: docker-wait
	$(ELK) filebeat -e setup

supervise-beats:
	$(ELK) service supervisor stop || true
	$(ELK) service supervisor start
	$(DOCKER_COMPOSE) exec web_1 service supervisor stop || true
	$(DOCKER_COMPOSE) exec web_1 service supervisor start
	$(DOCKER_COMPOSE) exec web_2 service supervisor stop || true
	$(DOCKER_COMPOSE) exec web_2 service supervisor start
	$(DOCKER_COMPOSE) exec web_3 service supervisor stop || true
	$(DOCKER_COMPOSE) exec web_3 service supervisor start
	$(DOCKER_COMPOSE) exec php_1 service supervisor stop || true
	$(DOCKER_COMPOSE) exec php_1 service supervisor start
	$(DOCKER_COMPOSE) exec php_2 service supervisor stop || true
	$(DOCKER_COMPOSE) exec php_2 service supervisor start

fix-mapping:
	$(DOCKER_COMPOSE) exec php_1 curl -XDELETE "http://elk:9200/filebeat-demo?pretty" -H 'Content-Type: application/json'
	$(DOCKER_COMPOSE) exec php_1 curl -XPUT "http://elk:9200/filebeat-demo?pretty" -H 'Content-Type: application/json' -d'{"settings" : {"index" : {"number_of_shards" : 3,"number_of_replicas" : 1}}}'
	$(DOCKER_COMPOSE) exec php_1 curl -XPUT "http://elk:9200/filebeat-demo/doc/_mapping?pretty" -H 'Content-Type: application/json' --data-binary "@/var/www/html/elk-demo/app/Resources/data/kibana_mapping.json"

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'