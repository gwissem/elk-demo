<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateCSVCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:csv:generate')
            ->setDescription('generate random fake fixtures CSV file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->generateAuthors();
        $this->generateBooks();
    }

    private final function generateAuthors()
    {

    }

    private final function generateBooks()
    {

    }
}
